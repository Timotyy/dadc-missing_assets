Attribute VB_Name = "DADCDumpAndSend"
Option Explicit
Public g_strConnection As String
Public cnn As ADODB.Connection

Const g_strSMTPServer = "jca-eset.jcatv.co.uk"
Const g_strSMTPUserName = "" ' "aria@freenetname.co.uk"
Const g_strSMTPPassword = "" '"Chorale1"
Const g_strEmailFooter = "Media Window at RRsat Europe"
Public g_strUseremailAddress As String
Public g_strAdministratorEmailAddress As String
Public g_strUseremailName As String
Private Declare Function OpenProcess Lib "kernel32" ( _
    ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" ( _
    ByVal hObject As Long) As Long
Private Declare Function EnumProcesses Lib "PSAPI.DLL" ( _
   lpidProcess As Long, ByVal cb As Long, cbNeeded As Long) As Long
Private Declare Function EnumProcessModules Lib "PSAPI.DLL" ( _
    ByVal hProcess As Long, lphModule As Long, ByVal cb As Long, lpcbNeeded As Long) As Long
Private Declare Function GetModuleBaseName Lib "PSAPI.DLL" Alias "GetModuleBaseNameA" ( _
    ByVal hProcess As Long, ByVal hModule As Long, ByVal lpFileName As String, ByVal nSize As Long) As Long
Private Const PROCESS_VM_READ = &H10
Private Const PROCESS_QUERY_INFORMATION = &H400
Public Const TC_UN = 0
Public Const TC_25 = 1
Public Const TC_29 = 2
Public Const TC_30 = 3
Public Const TC_24 = 4

Private Declare Function WNetGetConnection Lib _
   "mpr.dll" Alias "WNetGetConnectionA" (ByVal lpszLocalName _
   As String, ByVal lpszRemoteName As String, _
   cbRemoteName As Long) As Long

Private Declare Function GetComputerName Lib "kernel32" _
   Alias "GetComputerNameA" (ByVal lpBuffer As String, _
   nSize As Long) As Long

Private Const MAX_COMPUTERNAME_LENGTH As Long = 15&

' Reg Key ROOT Types...
Const HKEY_LOCAL_MACHINE = &H80000002
Const ERROR_SUCCESS = 0
Const REG_SZ = 1                         ' Unicode nul terminated string
Const REG_DWORD = 4                      ' 32-bit number

Private Declare Function RegOpenKey Lib "advapi32.dll" _
  Alias "RegOpenKeyA" (ByVal hKey As Long, _
  ByVal lpSubKey As String, phkResult As Long) As Long
   
Private Declare Function RegEnumValue Lib "advapi32.dll" _
   Alias "RegEnumValueA" (ByVal hKey As Long, ByVal dwIndex _
   As Long, ByVal lpValueName As String, lpcbValueName _
   As Long, ByVal lpReserved As Long, lpType As Long, _
   ByVal lpData As String, lpcbData As Long) As Long
   
Private Declare Function RegCloseKey Lib "advapi32.dll" _
  (ByVal hKey As Long) As Long
  
Public Sub DADCBBCTrackerDumpAndSend()

Dim l_strSelectionFormula As String, l_strReportFile As String
Dim FSO As Scripting.FileSystemObject, l_strExportName As String
Dim rs As ADODB.Recordset, SQL As String
Dim crxApplication As CRAXDRT.Application
Dim crxReport As CRAXDRT.Report
Dim Place As Integer

cnn.Open g_strConnection

Set rs = New ADODB.Recordset
SQL = "SELECT value FROM setting WHERE name = 'OperationsEmailAddress';"
rs.Open SQL, cnn, 3, 3
If rs.RecordCount > 0 Then
    g_strUseremailAddress = rs("value")
End If
rs.Close
SQL = "SELECT value FROM setting WHERE name = 'OperationsEmailName';"
rs.Open SQL, cnn, 3, 3
If rs.RecordCount > 0 Then
    g_strUseremailName = rs("value")
End If
rs.Close
SQL = "SELECT value FROM setting WHERE name = 'AdministratorEmailAddress';"
rs.Open SQL, cnn, 3, 3
If rs.RecordCount > 0 Then
    g_strAdministratorEmailAddress = rs("value")
End If
rs.Close
Set rs = Nothing

On Error GoTo ReportError

l_strSelectionFormula = "{tracker_dadc_item.companyID} = 1261"
l_strReportFile = App.Path & "\" & "DADC_report_lastcommentonly.rpt"
Place = 1
Set crxApplication = New CRAXDRT.Application
'Set crxReport = crxApplication.OpenReport(GetUNCNameNT(l_strReportFile))
Place = 2
Set crxReport = crxApplication.OpenReport(l_strReportFile)
Place = 3
crxReport.RecordSelectionFormula = l_strSelectionFormula
Place = 4
crxReport.DiscardSavedData

Set FSO = New Scripting.FileSystemObject
l_strExportName = "DADC_report_lastcommentonly"
Place = 5
If FSO.FileExists(App.Path & "\" & l_strExportName & ".xls") Then FSO.DeleteFile App.Path & "\" & l_strExportName & ".xls", True
Place = 6
If FSO.FileExists(App.Path & "\" & l_strExportName & ".zip") Then FSO.DeleteFile App.Path & "\" & l_strExportName & ".zip", True
crxReport.ExportOptions.DiskFileName = App.Path & "\" & l_strExportName & ".xls"
crxReport.ExportOptions.DestinationType = crEDTDiskFile
crxReport.ExportOptions.FormatType = crEFTExcelDataOnly
'MsgBox App.Path & "\" & l_strExportName & ".xls"
Place = 7
crxReport.Export False
Place = 8
Shell """C:\Program Files\7-Zip\7z.exe"" a """ & App.Path & "\" & l_strExportName & ".zip" & """ """ & App.Path & "\" & l_strExportName & ".xls" & """"

Sleep (10000)
SQL = "SELECT email, fullname FROM trackernotification WHERE contractgroup = 'DADC' and trackermessageID = 53;"
Set rs = New ADODB.Recordset
rs.Open SQL, cnn, 3, 3

If rs.RecordCount > 0 Then
    rs.MoveFirst
    While Not rs.EOF
        SendSMTPMail rs("email"), rs("fullname"), "RRmedia DADC BBC Tracker Data", "A Zipped RRmedia DADC BBC Tracker Data export is attached", App.Path & "\" & l_strExportName & ".zip", True, "", "", ""
        rs.MoveNext
    Wend
End If
rs.Close
Set rs = Nothing

Set crxReport = Nothing
crxApplication.CanClose
Set crxApplication = Nothing

'l_strSelectionFormula = "{tracker_svensk_item.companyID} = 1415"
'l_strReportFile = App.Path & "\" & "Svensk_report_lastcommentonly.rpt"
'
'Set crxApplication = New CRAXDRT.Application
''Set crxReport = crxApplication.OpenReport(GetUNCNameNT(l_strReportFile))
'Set crxReport = crxApplication.OpenReport(l_strReportFile)
'
'crxReport.RecordSelectionFormula = l_strSelectionFormula
'
'crxReport.DiscardSavedData
'
'Set FSO = New Scripting.FileSystemObject
'l_strExportName = "Svensk_report_lastcommentonly"
'If FSO.FileExists(App.Path & "\" & l_strExportName & ".xls") Then FSO.DeleteFile App.Path & "\" & l_strExportName & ".xls", True
'If FSO.FileExists(App.Path & "\" & l_strExportName & ".zip") Then FSO.DeleteFile App.Path & "\" & l_strExportName & ".zip", True
'crxReport.ExportOptions.DiskFileName = App.Path & "\" & l_strExportName & ".xls"
'crxReport.ExportOptions.DestinationType = crEDTDiskFile
'crxReport.ExportOptions.FormatType = crEFTExcelDataOnly
'crxReport.Export False
'If Create_Empty_Zip(App.Path & "\" & l_strExportName & ".zip") = True Then
'    Zip_Activity "ZIPFILE", App.Path & "\" & l_strExportName & ".xls", App.Path & "\" & l_strExportName & ".zip"
'    Sleep (10000)
'    SQL = "SELECT email, fullname FROM trackernotification WHERE contractgroup = 'SVENSK' and trackermessageID = 54;"
'    Set rs = New ADODB.Recordset
'    rs.Open SQL, cnn, 3, 3
'
'    If rs.RecordCount > 0 Then
'        rs.MoveFirst
'        While Not rs.EOF
'            SendSMTPMail rs("email"), rs("fullname"), "RRmedia DADC Svensk Tracker Data", "A Zipped RRmedia DADC BBC Tracker Svensk export is attached", App.Path & "\" & l_strExportName & ".zip", True, "", "", ""
'            rs.MoveNext
'        Wend
'    End If
'    rs.Close
'    Set rs = Nothing
'
'End If
'
'Set crxReport = Nothing
'crxApplication.CanClose
'Set crxApplication = Nothing

GoTo Close_And_Exit

ReportError:

MsgBox Err.Description & " at " & Place

Close_And_Exit:

cnn.Close
Exit Sub

End Sub

Private Function IsProcessRunning(ByVal sProcess As String) As Boolean

Const MAX_PATH As Long = 260

Dim lProcesses() As Long, lModules() As Long, N As Long, lRet As Long, hProcess As Long
Dim sName As String
        
sProcess = UCase$(sProcess)
ReDim lProcesses(1023) As Long
If EnumProcesses(lProcesses(0), 1024 * 4, lRet) Then
    For N = 0 To (lRet \ 4) - 1
        hProcess = OpenProcess(PROCESS_QUERY_INFORMATION Or PROCESS_VM_READ, 0, lProcesses(N))
        If hProcess Then
            ReDim lModules(1023)
            If EnumProcessModules(hProcess, lModules(0), 1024 * 4, lRet) Then
                sName = String$(MAX_PATH, vbNullChar)
                GetModuleBaseName hProcess, lModules(0), sName, MAX_PATH
                sName = Left$(sName, InStr(sName, vbNullChar) - 1)
                If Len(sName) = Len(sProcess) Then
                    If sProcess = UCase$(sName) Then IsProcessRunning = True: Exit Function
                End If
            End If
        End If
        CloseHandle hProcess
    Next N
End If
End Function
Function QuoteSanitise(lp_strText) As String
        '<EhHeader>
        On Error GoTo QuoteSanitise_Err
        '</EhHeader>


        Dim l_strNewText As String

        Dim l_strOldText As String

        Dim l_intLoop    As Integer

         l_strNewText = ""
    
         If IsNull(lp_strText) Then
            l_strOldText = ""
            l_strNewText = l_strOldText
            lp_strText = ""
        Else
            l_strOldText = lp_strText
        End If
    
        If InStr(lp_strText, "'") <> 0 Then
        
            For l_intLoop = 1 To Len(l_strOldText)
            
                If Mid(l_strOldText, l_intLoop, 1) = "'" Then
                    l_strNewText = l_strNewText & "''"
                Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
                End If
            
            Next

        Else
            l_strNewText = lp_strText
        End If
    
        If Right(l_strNewText, 1) = "/" Or Right(l_strNewText, 1) = "\" Then
            l_strNewText = Left(l_strNewText, Len(l_strNewText) - 1)
        End If
    
        QuoteSanitise = l_strNewText
    

        '<EhFooter>
        Exit Function

QuoteSanitise_Err:
        App.LogEvent Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.QuoteSanitise " & _
               "at line " & Erl
        Resume Next
        '</EhFooter>
End Function

Sub SendSMTPMail(ByVal lp_strEmailToAddress As String, ByVal lp_strEmailToName As String, ByVal lp_strSubject As String, ByVal lp_strBody As String, ByVal lp_strAttachment As String, _
ByVal lp_blnDontShowErrors As Boolean, ByVal lp_strCopyToEmailAddress As String, ByVal lp_strCopyToName As String, Optional ByVal lp_strBCCAddress As String, _
Optional lp_blnNoGlobalFooter As Boolean, Optional lp_strEmailFromAddress As String)

Dim SQL As String, cnn2 As ADODB.Connection

If lp_blnNoGlobalFooter = True Then
    'Nothing
Else
    lp_strBody = lp_strBody & vbCrLf & vbCrLf & g_strEmailFooter
End If

SQL = "INSERT INTO emailmessage (emailTo, emailCC, emailBCC, emailSubject, emailBodyPlain, emailAttachmentPath, emailFrom, cuser, muser, SendViaService) VALUES ("
SQL = SQL & "'" & QuoteSanitise(lp_strEmailToAddress) & "', "
SQL = SQL & IIf(lp_strCopyToEmailAddress <> "", "'" & QuoteSanitise(lp_strCopyToEmailAddress) & "', ", "Null, ")
SQL = SQL & IIf(lp_strBCCAddress <> "", "'" & QuoteSanitise(lp_strBCCAddress) & "', ", "Null, ")
SQL = SQL & "'" & QuoteSanitise(lp_strSubject) & "', "
SQL = SQL & "'" & QuoteSanitise(lp_strBody) & "', "
SQL = SQL & IIf(lp_strAttachment <> "", "'" & QuoteSanitise(lp_strAttachment) & "', ", "Null, ")
SQL = SQL & IIf(lp_strEmailFromAddress <> "", "'" & QuoteSanitise(lp_strEmailFromAddress) & "', ", "Null, ")
SQL = SQL & "'Aspera', "
SQL = SQL & "'Aspera', "
SQL = SQL & "1);"

'On Error Resume Next
Debug.Print SQL
Set cnn2 = New ADODB.Connection
cnn2.Open g_strConnection
cnn2.Execute SQL
cnn2.Close
Set cnn2 = Nothing
'On Error GoTo 0

End Sub

Public Function FormatSQLDate(lDate As Variant)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If IsNull(lDate) Then
        FormatSQLDate = Null
        Exit Function
    End If
    If lDate = "" Then
        FormatSQLDate = Null
        Exit Function
    End If
    If Not IsDate(lDate) Then
        FormatSQLDate = Null
        Exit Function
    
    End If
    
    FormatSQLDate = Year(lDate) & "/" & Month(lDate) & "/" & Day(lDate) & " " & Hour(lDate) & ":" & Minute(lDate) & ":" & Second(lDate)
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    App.LogEvent Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetData(lp_strTableName As String, lp_strFieldToReturn As String, lp_strFieldToSearch As String, lp_varCriterea As Variant) As Variant
        '<EhHeader>
        On Error GoTo GetData_Err
        '</EhHeader>


    Dim l_strSQL As String
    l_strSQL = "SELECT " & lp_strFieldToReturn & " FROM " & lp_strTableName & " WHERE " & lp_strFieldToSearch & " = '" & (lp_varCriterea) & "'"

    Dim l_rstGetData As New ADODB.Recordset
    Dim l_con As New ADODB.Connection

    l_con.Open g_strConnection
    l_rstGetData.Open l_strSQL, l_con, adOpenStatic, adLockReadOnly

    If Not l_rstGetData.EOF Then
    
        If Not IsNull(l_rstGetData(lp_strFieldToReturn)) Then
            GetData = l_rstGetData(lp_strFieldToReturn)
            GoTo Proc_CloseDB
        End If

    Else
        GoTo Proc_CloseDB

    End If

    Select Case l_rstGetData.Fields(lp_strFieldToReturn).Type
    Case adChar, adVarChar, adVarWChar, 201, 203
        GetData = ""
    Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
        GetData = 0
    Case adDate, adDBDate, adDBTime, adDBTimeStamp
        GetData = 0
    Case Else
        GetData = False
    End Select

Proc_CloseDB:

    On Error Resume Next

    l_rstGetData.Close
    Set l_rstGetData = Nothing

    l_con.Close
    Set l_con = Nothing


        '<EhFooter>
        Exit Function

GetData_Err:
        App.LogEvent Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.GetData " & _
               "at line " & Erl
        Resume Next
        '</EhFooter>
End Function

Public Function URLEncode(ByVal s As String) As String

    Dim lLoop As Long
    Dim sC As String

    URLEncode = ""
    For lLoop = 1 To Len(s)
        sC = Mid$(s, lLoop, 1)
        If InStr("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", UCase$(sC)) = 0 Then
            If Asc(sC) < 16 Then
                sC = "%0" & Hex$(Asc(sC))
            Else
                sC = "%" & Hex$(Asc(sC))
            End If
        End If
        
        URLEncode = URLEncode & sC
    Next
    
End Function

Public Sub Main()

Initialise_cnn
DADCBBCTrackerDumpAndSend

End Sub

Public Sub Initialise_cnn()

Dim CommandString As String
CommandString = Command()

g_strConnection = "DRIVER={ODBC Driver 13 for SQL Server};SERVER=lon-bssql-01.office.refine-group.com;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"

'g_strConnection = "DRIVER=SQL Server;SERVER=jca-sql-1.jcatv.co.uk;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"
'g_strConnection = "DRIVER={ODBC Driver 13 for SQL Server};SERVER=MX1TEST-sql-3;Failover_Partner=MX1TEST-sql-4;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"

Set cnn = New ADODB.Connection
       
End Sub

Public Function GetUNCNameNT(pathName As String) As String

Dim hKey As Long
Dim hKey2 As Long
Dim exitFlag As Boolean
Dim i As Double
Dim errCode As Long
Dim rootKey As String
Dim Key As String
Dim computerName As String
Dim lComputerName As Long
Dim stPath As String
Dim firstLoop As Boolean
Dim Ret As Boolean

' first, verify whether the disk is connected to the network
If Mid(pathName, 2, 1) = ":" Then
   Dim UNCName As String
   Dim lenUNC As Long

   UNCName = String$(520, 0)
   lenUNC = 520
   errCode = WNetGetConnection(Left(pathName, 2), UNCName, lenUNC)

   If errCode = 0 Then
      UNCName = Trim(Left$(UNCName, InStr(UNCName, _
        vbNullChar) - 1))
      GetUNCNameNT = UNCName & Mid(pathName, 3)
      Exit Function
   End If
End If

' else, scan the registry looking for shared resources
'(NT version)
computerName = String$(255, 0)
lComputerName = Len(computerName)
errCode = GetComputerName(computerName, lComputerName)
If errCode <> 1 Then
   GetUNCNameNT = pathName
   Exit Function
End If

computerName = Trim(Left$(computerName, InStr(computerName, _
   vbNullChar) - 1))
rootKey = "SYSTEM\CurrentControlSet\Services\LanmanServer\Shares"
errCode = RegOpenKey(HKEY_LOCAL_MACHINE, rootKey, hKey)

If errCode <> 0 Then
   GetUNCNameNT = pathName
   Exit Function
End If

firstLoop = True

Do Until exitFlag
   Dim szValue As String
   Dim szValueName As String
   Dim cchValueName As Long
   Dim dwValueType As Long
   Dim dwValueSize As Long

   szValueName = String(1024, 0)
   cchValueName = Len(szValueName)
   szValue = String$(500, 0)
   dwValueSize = Len(szValue)

   ' loop on "i" to access all shared DLLs
   ' szValueName will receive the key that identifies an element
   errCode = RegEnumValue(hKey, i#, szValueName, _
       cchValueName, 0, dwValueType, szValue, dwValueSize)

   If errCode <> 0 Then
      If Not firstLoop Then
         exitFlag = True
      Else
         i = -1
         firstLoop = False
      End If
   Else
      stPath = GetPath(szValue)
      If firstLoop Then
         Ret = (UCase(stPath) = UCase(pathName))
         stPath = ""
      Else
         Ret = (UCase(stPath) = UCase(Left$(pathName, _
        Len(stPath))))
         stPath = Mid$(pathName, Len(stPath))
      End If
      If Ret Then
         exitFlag = True
         szValueName = Left$(szValueName, cchValueName)
         GetUNCNameNT = "\\" & computerName & "\" & _
            szValueName & stPath
      End If
   End If
   i = i + 1
Loop

RegCloseKey hKey
If GetUNCNameNT = "" Then GetUNCNameNT = pathName

End Function

Private Function GetPath(st As String) As String
   Dim pos1 As Long, pos2 As Long, pos3 As Long
   Dim stPath As String

   pos1 = InStr(st, "Path")
   If pos1 > 0 Then
      pos2 = InStr(pos1, st, vbNullChar)
      stPath = Mid$(st, pos1, pos2 - pos1)
      pos3 = InStr(stPath, "=")
      If pos3 > 0 Then
         stPath = Mid$(stPath, pos3 + 1)
         GetPath = stPath
      End If
   End If
End Function



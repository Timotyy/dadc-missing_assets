Attribute VB_Name = "DADCMissingAssets_Process_Module"
Option Explicit
Public g_strConnection As String
Public cnn As ADODB.Connection

Const g_strSMTPServer = "jca-aspera.jcatv.co.uk"
Const g_strSMTPUserName = "" ' "aria@freenetname.co.uk"
Const g_strSMTPPassword = "" '"Chorale1"
Const g_strEmailFooter = "MX1 Ltd. London"
Public g_strUseremailAddress As String
Public g_strAdministratorEmailAddress As String
Public g_strUseremailName As String
Private Declare Function OpenProcess Lib "kernel32" ( _
    ByVal dwDesiredAccess As Long, ByVal bInheritHandle As Long, ByVal dwProcessId As Long) As Long
Private Declare Function CloseHandle Lib "kernel32" ( _
    ByVal hObject As Long) As Long
Private Declare Function EnumProcesses Lib "PSAPI.DLL" ( _
   lpidProcess As Long, ByVal cb As Long, cbNeeded As Long) As Long
Private Declare Function EnumProcessModules Lib "PSAPI.DLL" ( _
    ByVal hProcess As Long, lphModule As Long, ByVal cb As Long, lpcbNeeded As Long) As Long
Private Declare Function GetModuleBaseName Lib "PSAPI.DLL" Alias "GetModuleBaseNameA" ( _
    ByVal hProcess As Long, ByVal hModule As Long, ByVal lpFileName As String, ByVal nSize As Long) As Long
Private Const PROCESS_VM_READ = &H10
Private Const PROCESS_QUERY_INFORMATION = &H400
Public Const TC_UN = 0
Public Const TC_25 = 1
Public Const TC_29 = 2
Public Const TC_30 = 3
Public Const TC_24 = 4

Public Sub DADCPendingItemsProcess()

Dim l_strSQL As String, l_strLocation As String, l_datInterested As Date, l_datInterested2 As Date
Dim l_rsDADCTracker As ADODB.Recordset, l_rsDADCcomments As ADODB.Recordset, l_strBarcode As String, l_strEmailBody As String, l_strEmailBody2 As String, l_strTapeFormat As String, l_rst As ADODB.Recordset
Dim l_lngSec As Long, l_lngHour As Long, SQL As String, SectionTitle As String
Dim l_lngLastProjectManagerContactID As Long, l_strEmailTo As String, l_strEmailToName As String
Dim l_lngLastCompanyID As Long, l_rsEmail As ADODB.Recordset

'On Error GoTo ErrorNotification

cnn.Open g_strConnection

Set l_rsDADCTracker = New ADODB.Recordset
SQL = "SELECT value FROM setting WHERE name = 'OperationsEmailAddress';"
l_rsDADCTracker.Open SQL, cnn, 3, 3
If l_rsDADCTracker.RecordCount > 0 Then
    g_strUseremailAddress = l_rsDADCTracker("value")
End If
l_rsDADCTracker.Close
SQL = "SELECT value FROM setting WHERE name = 'OperationsEmailName';"
l_rsDADCTracker.Open SQL, cnn, 3, 3
If l_rsDADCTracker.RecordCount > 0 Then
    g_strUseremailName = l_rsDADCTracker("value")
End If
l_rsDADCTracker.Close
SQL = "SELECT value FROM setting WHERE name = 'AdministratorEmailAddress';"
l_rsDADCTracker.Open SQL, cnn, 3, 3
If l_rsDADCTracker.RecordCount > 0 Then
    g_strAdministratorEmailAddress = l_rsDADCTracker("value")
End If
l_rsDADCTracker.Close
Set l_rsDADCTracker = Nothing

Set l_rsDADCTracker = New ADODB.Recordset
Set l_rsDADCcomments = New ADODB.Recordset
Set l_rsEmail = New ADODB.Recordset

'GoTo TESTINGPOINT
SectionTitle = "BBCMG Tracker Missing Tape Checks : All Requested Items for which assets have not yet arrived, sorted by Due Date ASC"
l_strEmailBody = ""

l_strSQL = "SELECT * FROM vw_BBCMG_WaitingForTape "
l_strSQL = l_strSQL & "ORDER BY Due_Date;"
Debug.Print l_strSQL
l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
If l_rsDADCTracker.RecordCount > 0 Then
    l_rsDADCTracker.MoveFirst
    l_strEmailBody = "Tapes that have not yet arrived at RR Media."
    Do While Not l_rsDADCTracker.EOF
        l_strEmailBody = l_strEmailBody & vbCrLf & vbCrLf & "Request # " & l_rsDADCTracker("Request_Number")
        If Trim(" " & l_rsDADCTracker("Tape_Number")) <> "" Then l_strEmailBody = l_strEmailBody & ", Barcode: " & l_rsDADCTracker("Tape_Number")
        If Not IsNull(l_rsDADCTracker("Due_Date")) Then l_strEmailBody = l_strEmailBody & ", Deadline Date: " & l_rsDADCTracker("Due_Date")
        l_strEmailBody = l_strEmailBody & vbCrLf & "Title: " & Trim(" " & l_rsDADCTracker("Program_Title"))
        l_rsDADCTracker.MoveNext
    Loop
End If
l_rsDADCTracker.Close
        
Debug.Print l_strEmailBody

SectionTitle = "BBCMG Tracker Missing File Checks : All Requested Items for which assets have not yet arrived, sorted by Due Date ASC"
l_strSQL = "SELECT * FROM vw_BBCMG_WaitingForFile "
l_strSQL = l_strSQL & "ORDER BY Due_Date;"
Debug.Print l_strSQL
l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
If l_rsDADCTracker.RecordCount > 0 Then
    l_rsDADCTracker.MoveFirst
    l_strEmailBody = l_strEmailBody & vbCrLf & vbCrLf & "Files that have not yet arrived at RR Media."
    Do While Not l_rsDADCTracker.EOF
        l_strEmailBody = l_strEmailBody & vbCrLf & vbCrLf & "Request # " & l_rsDADCTracker("Request_Number")
        If Trim(" " & l_rsDADCTracker("Tape_Number")) <> "" Then l_strEmailBody = l_strEmailBody & ", File_Name: " & l_rsDADCTracker("Tape_Number")
        If Not IsNull(l_rsDADCTracker("Due_Date")) Then l_strEmailBody = l_strEmailBody & ", Deadline Date: " & l_rsDADCTracker("Due_Date")
        l_strEmailBody = l_strEmailBody & vbCrLf & "Title: " & Trim(" " & l_rsDADCTracker("Program_Title"))
        l_rsDADCTracker.MoveNext
    Loop
End If
l_rsDADCTracker.Close
        
Debug.Print l_strEmailBody

If l_strEmailBody <> "" Then

    l_strEmailBody = "The following items have been ordered for Preview Portals, but the source materials have not yet arrived at MX1. The List is sorted with most urgent first:" & l_strEmailBody
    l_strSQL = "SELECT * FROM trackernotification WHERE contractgroup = 'BBC' AND trackermessageID = 52;"
    l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
    If l_rsDADCTracker.RecordCount > 0 Then
        l_rsDADCTracker.MoveFirst
        While Not l_rsDADCTracker.EOF
            SendSMTPMail l_rsDADCTracker("email"), l_rsDADCTracker("fullname"), "BBCMG Preview Portals Missing Assets", l_strEmailBody, "", True, "", "", ""
            l_rsDADCTracker.MoveNext
        Wend
    End If
    l_rsDADCTracker.Close

End If

SectionTitle = "BBCWW Tracker Missing Asset Checks : All Requested Items for which assets have not yet arrived, sorted by Due Date ASC"
Set l_rst = cnn.Execute("exec fn_getWorkingDaysBackFromDate @fromdate='" & FormatSQLDate(Now) & "', @workingdays=3")
l_datInterested = l_rst(0)
l_rst.Close
Set l_rst = cnn.Execute("exec fn_getWorkingDaysBackFromDate @fromdate='" & FormatSQLDate(Now) & "', @workingdays=1")
l_datInterested2 = l_rst(0)
l_rst.Close

l_strEmailBody = ""

l_strSQL = "SELECT * FROM vw_Tracker_BBCWW "
l_strSQL = l_strSQL & "WHERE (DateMasterArrived IS NULL OR (DateMasterArrived IS NOT NULL AND DateTapeSentBack IS NOT NULL)) AND completeddate IS NULL AND nochargedate IS NULL AND cancelleddate IS NULL AND invoiceddate IS NULL AND rejecteddate IS NULL AND companyID = 570 ORDER BY deadlinedate;"
Debug.Print l_strSQL
l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
If l_rsDADCTracker.RecordCount > 0 Then
    l_rsDADCTracker.MoveFirst
    Do While Not l_rsDADCTracker.EOF
        l_strEmailBody = l_strEmailBody & vbCrLf & vbCrLf & "Job# " & l_rsDADCTracker("jobID")
        If Trim(" " & l_rsDADCTracker("MasterBarcode")) <> "" Then l_strEmailBody = l_strEmailBody & ", Barcode: " & l_rsDADCTracker("MasterBarcode")
        If Not IsNull(l_rsDADCTracker("deadlinedate")) Then
            l_strEmailBody = l_strEmailBody & ", Deadline Date: " & l_rsDADCTracker("deadlinedate")
            If l_rsDADCTracker("Priority") = 0 Then
                If l_rsDADCTracker("deadlinedate") < l_datInterested Then l_strEmailBody = l_strEmailBody & " (Cannot now be completed within SLA)"
            Else
                If l_rsDADCTracker("deadlinedate") < l_datInterested2 Then l_strEmailBody = l_strEmailBody & " (Cannot now be completed within SLA)"
            End If
        End If
        If l_rsDADCTracker("priority") <> 0 Then
            l_strEmailBody = l_strEmailBody & vbCrLf & "Item marked as PRIORITY!"
        End If
        l_strEmailBody = l_strEmailBody & vbCrLf & "Title: " & Trim(" " & l_rsDADCTracker("description"))
'        If Trim(" " & l_rsDADCTracker("audioconfiguration")) <> "" Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Requirements: " & l_rsDADCTracker("audioconfiguration")
'        End If
'        l_strEmailBody = l_strEmailBody & vbCrLf & "Link to Web Tracker: http://online.rrsat.tv/dadctracker/tracker_details.asp?TrackerID=" & URLEncode(l_rsDADCTracker("tracker_dadc_itemID")) & "&goback=tracker" & vbCrLf & vbCrLf
        l_rsDADCTracker.MoveNext
    Loop
End If
l_rsDADCTracker.Close
        
Debug.Print l_strEmailBody

If l_strEmailBody <> "" Then

    l_strEmailBody = "The following items are listed in the BBCWW tracker, but the source materials have not yet arrived at MX1. The List is sorted with most urgent first:" & l_strEmailBody
'    l_strEmailBody = l_strEmailBody & "When accessing the Web Tracker, please be sure to log in with the correct credentials for that particular Tracker." & vbCrLf
    l_strSQL = "SELECT * FROM trackernotification WHERE contractgroup = 'BBC' AND trackermessageID = 51;"
    l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
    If l_rsDADCTracker.RecordCount > 0 Then
        l_rsDADCTracker.MoveFirst
        While Not l_rsDADCTracker.EOF
            SendSMTPMail l_rsDADCTracker("email"), l_rsDADCTracker("fullname"), "BBCWW Tracker Missing Assets", l_strEmailBody, "", True, "", "", ""
            l_rsDADCTracker.MoveNext
        Wend
    End If
    l_rsDADCTracker.Close

End If

SectionTitle = "DADC BBC Tracker Missing Asset Checks : All Requested Items for which assets have not yet arrived, sorted by Due Date ASC"
l_datInterested = DateAdd("d", 5, Now)
l_strEmailBody = ""

l_strSQL = "SELECT * FROM tracker_dadc_item "
l_strSQL = l_strSQL & "WHERE (stagefield1 IS NULL OR (stagefield1 IS NOT NULL AND stagefield14 IS NOT NULL and stagefield2 IS NULL)) AND readytobill = 0 AND billed = 0 AND companyID = 1261 AND rejected = 0 ORDER BY duedateupload;"
Debug.Print l_strSQL
l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
If l_rsDADCTracker.RecordCount > 0 Then
    l_rsDADCTracker.MoveFirst
    Do While Not l_rsDADCTracker.EOF
        l_strEmailBody = l_strEmailBody & vbCrLf & vbCrLf & "ContentVersionCode: " & l_rsDADCTracker("contentversioncode")
        If Trim(" " & l_rsDADCTracker("newbarcode")) <> "" Then l_strEmailBody = l_strEmailBody & ", Barcode: " & l_rsDADCTracker("newbarcode")
        If Not IsNull(l_rsDADCTracker("duedateupload")) Then
            l_strEmailBody = l_strEmailBody & ", Due Date: " & l_rsDADCTracker("duedateupload")
        End If
        l_strEmailBody = l_strEmailBody & vbCrLf & "Title: " & Trim(" " & l_rsDADCTracker("Title"))
        If Trim(" " & l_rsDADCTracker("Episode")) <> "" Then l_strEmailBody = l_strEmailBody & ", Episode #: " & l_rsDADCTracker("Episode")
        If Trim(" " & l_rsDADCTracker("subtitle")) <> "" Then l_strEmailBody = l_strEmailBody & vbCrLf & "Episode Title: " & l_rsDADCTracker("subtitle")
        If Trim(" " & l_rsDADCTracker("language")) <> "" Then
            l_strEmailBody = l_strEmailBody & vbCrLf & "Language: " & l_rsDADCTracker("language")
        End If
        If l_rsDADCTracker("priority") <> 0 Then
            l_strEmailBody = l_strEmailBody & vbCrLf & "Item marked as PRIORITY!"
        End If
'        If Trim(" " & l_rsDADCTracker("audioconfiguration")) <> "" Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Requirements: " & l_rsDADCTracker("audioconfiguration")
'        End If
'        l_strEmailBody = l_strEmailBody & vbCrLf & "Link to Web Tracker: http://online.rrsat.tv/dadctracker/tracker_details.asp?TrackerID=" & URLEncode(l_rsDADCTracker("tracker_dadc_itemID")) & "&goback=tracker" & vbCrLf & vbCrLf
        l_rsDADCTracker.MoveNext
    Loop
End If
l_rsDADCTracker.Close
        
Debug.Print l_strEmailBody

If l_strEmailBody <> "" Then

    l_strEmailBody = "The following items are listed in the BBC tracker, but the source materials have not yet arrived at MX1. The List is sorted with most urgent first:" & l_strEmailBody
'    l_strEmailBody = l_strEmailBody & "When accessing the Web Tracker, please be sure to log in with the correct credentials for that particular Tracker." & vbCrLf
    l_strSQL = "SELECT * FROM trackernotification WHERE contractgroup = 'DADC' AND trackermessageID = 47;"
    l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
    If l_rsDADCTracker.RecordCount > 0 Then
        l_rsDADCTracker.MoveFirst
        While Not l_rsDADCTracker.EOF
            SendSMTPMail l_rsDADCTracker("email"), l_rsDADCTracker("fullname"), "DADC BBC Tracker Missing Assets", l_strEmailBody, "", True, "", "", ""
            l_rsDADCTracker.MoveNext
        Wend
    End If
    l_rsDADCTracker.Close

End If

'SectionTitle = "DADC Svensk Compilations Tracker Missing Asset Checks : within 5 days of deadline"
'l_datInterested = DateAdd("d", 5, Now)
'l_strEmailBody = ""
'
'l_strSQL = "SELECT * FROM tracker_svensk_compilations_item "
'l_strSQL = l_strSQL & "WHERE datearrived IS NULL AND duedate <= '" & FormatSQLDate(l_datInterested) & "' AND readytobill = 0 AND billed = 0 and companyID > 100 ORDER BY urgent, duedate;"
'Debug.Print l_strSQL
'l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
'If l_rsDADCTracker.RecordCount > 0 Then
'    l_rsDADCTracker.MoveFirst
'    Do While Not l_rsDADCTracker.EOF
'        l_strSQL = "SELECT * FROM tracker_svensk_compilations_comment WHERE CompilationTitle = '" & QuoteSanitise(l_rsDADCTracker("CompilationTitle")) & "' ORDER BY cdate;"
'        l_rsDADCcomments.Open l_strSQL, cnn, 3, 3
'        l_strEmailBody = l_strEmailBody & GetData("company", "portalcompanyname", "companyID", l_rsDADCTracker("companyID"))
'        If Not IsNull(l_rsDADCTracker("duedate")) Then
'            l_strEmailBody = l_strEmailBody & " Due Date: " & l_rsDADCTracker("duedate")
'        End If
'        l_strEmailBody = l_strEmailBody & vbCrLf & "Compilation Title: " & Trim(" " & l_rsDADCTracker("CompilationTitle"))
'        If Trim(" " & l_rsDADCTracker("SeasonNo")) <> "" Then l_strEmailBody = l_strEmailBody & " Season #: " & l_rsDADCTracker("SeasonNo")
'        If Trim(" " & l_rsDADCTracker("VolNo")) <> "" Then l_strEmailBody = l_strEmailBody & " Vol #: " & l_rsDADCTracker("VolNo")
'        If Trim(" " & l_rsDADCTracker("EpisodeNo")) <> "" Then l_strEmailBody = l_strEmailBody & vbCrLf & "Episode #: " & l_rsDADCTracker("EpisodeNo")
'        If Trim(" " & l_rsDADCTracker("EpisodeName")) <> "" Then l_strEmailBody = l_strEmailBody & vbCrLf & "Episode Name: " & l_rsDADCTracker("EpisodeName")
'        If Not IsNull(l_rsDADCTracker("language")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Language: " & l_rsDADCTracker("language")
'        End If
'        If l_rsDADCcomments.RecordCount > 0 Then
'            l_rsDADCcomments.MoveLast
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Last Comment (" & l_rsDADCcomments("cdate") & "): " & l_rsDADCcomments("comment")
'        End If
'        l_rsDADCcomments.Close
'        If l_rsDADCTracker("urgent") <> 0 Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Item marked as URGENT!"
'        End If
'        If UCase(l_rsDADCTracker("componenttype")) = "AUDIO" And IsNull(l_rsDADCTracker("datearrived")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Audio File is Missing."
'        End If
'        If UCase(l_rsDADCTracker("componenttype")) = "SUBTITLE" And IsNull(l_rsDADCTracker("datearrived")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Subititle File is Missing."
'        End If
'        If ((UCase(l_rsDADCTracker("componenttype")) = "VIDEO" Or UCase(l_rsDADCTracker("componenttype")) = "TRAILER") And IsNull(l_rsDADCTracker("datearrived"))) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Video File is Missing."
'        End If
'        l_strEmailBody = l_strEmailBody & vbCrLf & "Link to Web Tracker: http://online.rrmedia.tv/svensktracker/compilation_tracker_details.asp?CompilationTitle=" & URLEncode(l_rsDADCTracker("CompilationTitle")) & "&goback=tracker" & vbCrLf & vbCrLf
'        l_rsDADCTracker.MoveNext
'    Loop
'End If
'l_rsDADCTracker.Close
'
'Debug.Print l_strEmailBody
'
'If l_strEmailBody <> "" Then
'
'    l_strEmailBody = "The following items have a due date which is now passed, please amend due date and advise on delivery of assets:" & vbCrLf & vbCrLf & l_strEmailBody
'    l_strEmailBody = l_strEmailBody & "When accessing the Web Tracker, please be sure to log in with the correct credentials for that particular Tracker." & vbCrLf
'    l_strSQL = "SELECT * FROM trackernotification WHERE contractgroup = 'SVENSK' AND trackermessageID = 43;"
'    l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
'    If l_rsDADCTracker.RecordCount > 0 Then
'        l_rsDADCTracker.MoveFirst
'        While Not l_rsDADCTracker.EOF
'            SendSMTPMail l_rsDADCTracker("email"), l_rsDADCTracker("fullname"), "Compilations Tracker Missing Assets", l_strEmailBody, "", True, "", "", ""
'            l_rsDADCTracker.MoveNext
'        Wend
'    End If
'    l_rsDADCTracker.Close
'
'End If
'
'SectionTitle = "DADC Svensk Tracker Missing Asset Checks : DADC and Internal - within 5 days of deadline"
'l_datInterested = DateAdd("d", 5, Now)
'l_strEmailBody = ""
'
'l_strSQL = "SELECT * FROM tracker_svensk_item "
'l_strSQL = l_strSQL & "WHERE assetshere = 0 AND duedate <= '" & FormatSQLDate(l_datInterested) & "' AND readytobill = 0 AND billed = 0 and companyID > 100 ORDER BY urgent, duedate;"
'Debug.Print l_strSQL
'l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
'If l_rsDADCTracker.RecordCount > 0 Then
'    l_rsDADCTracker.MoveFirst
'    While Not l_rsDADCTracker.EOF
'        l_strSQL = "SELECT * FROM tracker_svensk_comment WHERE tracker_svensk_itemID = " & l_rsDADCTracker("tracker_svensk_itemID") & "ORDER BY cdate;"
'        l_rsDADCcomments.Open l_strSQL, cnn, 3, 3
'        l_strEmailBody = l_strEmailBody & GetData("company", "portalcompanyname", "companyID", l_rsDADCTracker("companyID"))
'        If Not IsNull(l_rsDADCTracker("itemreference")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Reference: " & l_rsDADCTracker("itemreference")
'        End If
'        If Not IsNull(l_rsDADCTracker("barcode")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Barcode: " & l_rsDADCTracker("barcode")
'        End If
'        If Not IsNull(l_rsDADCTracker("duedate")) Then
'            l_strEmailBody = l_strEmailBody & " Due Date: " & l_rsDADCTracker("duedate")
'        End If
'        l_strEmailBody = l_strEmailBody & vbCrLf & "Title: " & Trim(" " & l_rsDADCTracker("title")) & " - " & Trim(" " & l_rsDADCTracker("programtype"))
'        If Trim(" " & l_rsDADCTracker("series")) <> "" Then l_strEmailBody = l_strEmailBody & " Series #: " & l_rsDADCTracker("series")
'        If Trim(" " & l_rsDADCTracker("episode")) <> "" Then l_strEmailBody = l_strEmailBody & vbCrLf & "Episode #: " & l_rsDADCTracker("episode")
'        If Not IsNull(l_rsDADCTracker("language")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Language: " & l_rsDADCTracker("language")
'        End If
'        If l_rsDADCcomments.RecordCount > 0 Then
'            l_rsDADCcomments.MoveLast
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Last Comment (" & l_rsDADCcomments("cdate") & "): " & l_rsDADCcomments("comment")
'        End If
'        l_rsDADCcomments.Close
'        If l_rsDADCTracker("urgent") <> 0 Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Item marked as URGENT!"
'        End If
'        If UCase(l_rsDADCTracker("componenttype")) = "AUDIO" And IsNull(l_rsDADCTracker("masterarrived")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Audio File is Missing."
'        End If
'        If UCase(l_rsDADCTracker("componenttype")) = "SUBTITLE" And IsNull(l_rsDADCTracker("masterarrived")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Subititle File is Missing."
'        End If
'        If ((UCase(l_rsDADCTracker("componenttype")) = "VIDEO" Or UCase(l_rsDADCTracker("componenttype")) = "TRAILER") And IsNull(l_rsDADCTracker("masterarrived"))) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Video File is Missing."
'        End If
'        l_strEmailBody = l_strEmailBody & vbCrLf & "Link to Web Tracker: http://online.rrmedia.tv/svensktracker/tracker_details.asp?trackerID=" & l_rsDADCTracker("tracker_svensk_itemID") & "&goback=tracker" & vbCrLf & vbCrLf
'        l_rsDADCTracker.MoveNext
'    Wend
'End If
'l_rsDADCTracker.Close
'
'Debug.Print l_strEmailBody
'
'If l_strEmailBody <> "" Then
'
'    l_strEmailBody = "The following items have a due date which has passed, please amend due date and advise on delivery of assets:" & vbCrLf & vbCrLf & l_strEmailBody
'    l_strEmailBody = l_strEmailBody & "When accessing the Web Tracker, please be sure to log in with the correct credentials for that particular Tracker." & vbCrLf
'    l_strSQL = "SELECT * FROM trackernotification WHERE contractgroup = 'SVENSK' AND trackermessageID = 29;"
'    l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
'    If l_rsDADCTracker.RecordCount > 0 Then
'        l_rsDADCTracker.MoveFirst
'        While Not l_rsDADCTracker.EOF
'            SendSMTPMail l_rsDADCTracker("email"), l_rsDADCTracker("fullname"), "Missing Assets � Overdue", l_strEmailBody, "", True, "", "", ""
'            l_rsDADCTracker.MoveNext
'        Wend
'    End If
'    l_rsDADCTracker.Close
'
'End If
'
'SectionTitle = "DADC Svensk Tracker Missing Asset Checks : Svensk Project Managers - within 5 days of deadline"
'l_strEmailBody = ""
'
'l_strSQL = "SELECT * FROM tracker_svensk_item "
'l_strSQL = l_strSQL & "WHERE assetshere = 0 and duedate <= '" & FormatSQLDate(l_datInterested) & "' AND readytobill = 0 AND billed = 0 AND companyID = 1415 "
'l_strSQL = l_strSQL & "ORDER BY projectmanagercontactID, urgent, duedate;"
'Debug.Print l_strSQL
'l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
'If l_rsDADCTracker.RecordCount > 0 Then
'    l_rsDADCTracker.MoveFirst
'    l_lngLastProjectManagerContactID = Val(Trim(" " & l_rsDADCTracker("projectmanagercontactID")))
'    While Not l_rsDADCTracker.EOF
'        If Val(Trim(" " & l_rsDADCTracker("projectmanagercontactID"))) <> l_lngLastProjectManagerContactID Then
'            'Send the email to the last project manager
'            Debug.Print l_strEmailBody
'            If l_strEmailBody <> "" Then
'                l_strEmailBody = "The following items have a due date which has passed, please amend due date and advise on delivery of assets:" & vbCrLf & vbCrLf & l_strEmailBody
'                l_strEmailBody = l_strEmailBody & "When accessing the Web Tracker, please be sure to log in with the correct credentials for that particular Tracker." & vbCrLf
'                If l_lngLastProjectManagerContactID <> 0 Then
'                    l_strEmailTo = GetData("contact", "email", "contactID", l_lngLastProjectManagerContactID)
'                    l_strEmailToName = GetData("contact", "name", "contactID", l_lngLastProjectManagerContactID)
'                    SendSMTPMail l_strEmailTo, l_strEmailToName, "Missing Assets � Overdue", l_strEmailBody, "", True, "coreswe@sf.se", "coreswe@sf.se", "", g_strAdministratorEmailAddress
'                Else
'                    'Send to coreSF address? coreswe@sf.se
'                    SendSMTPMail "coreswe@sf.se", "coreswe@sf.se", "Missing Assets � Overdue", l_strEmailBody, "", True, "", "", "", g_strAdministratorEmailAddress
'                End If
'            End If
'            l_lngLastProjectManagerContactID = l_rsDADCTracker("projectmanagercontactID")
'            l_strEmailBody = ""
'        End If
'        l_strSQL = "SELECT * FROM tracker_svensk_comment WHERE tracker_svensk_itemID = " & l_rsDADCTracker("tracker_svensk_itemID") & "ORDER BY cdate;"
'        l_rsDADCcomments.Open l_strSQL, cnn, 3, 3
'        l_strEmailBody = l_strEmailBody & GetData("company", "name", "companyID", l_rsDADCTracker("companyID"))
'        If Not IsNull(l_rsDADCTracker("itemreference")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Reference: " & l_rsDADCTracker("itemreference")
'        End If
'        If Not IsNull(l_rsDADCTracker("barcode")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Barcode: " & l_rsDADCTracker("barcode")
'        End If
'        If Not IsNull(l_rsDADCTracker("duedate")) Then
'            l_strEmailBody = l_strEmailBody & " Due Date: " & l_rsDADCTracker("duedate")
'        End If
'        l_strEmailBody = l_strEmailBody & vbCrLf & "Title: " & Trim(" " & l_rsDADCTracker("title")) & " - " & Trim(" " & l_rsDADCTracker("programtype"))
'        If Trim(" " & l_rsDADCTracker("series")) <> "" Then l_strEmailBody = l_strEmailBody & " Series #: " & l_rsDADCTracker("series")
'        If Trim(" " & l_rsDADCTracker("episode")) <> "" Then l_strEmailBody = l_strEmailBody & vbCrLf & "Episode #: " & l_rsDADCTracker("episode")
'        If Not IsNull(l_rsDADCTracker("language")) Then
'            l_strEmailBody = l_strEmailBody & " Language: " & l_rsDADCTracker("language")
'        End If
'        If l_rsDADCcomments.RecordCount > 0 Then
'            l_rsDADCcomments.MoveLast
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Last Comment (" & l_rsDADCcomments("cdate") & "): " & l_rsDADCcomments("comment")
'        End If
'        l_rsDADCcomments.Close
'        If l_rsDADCTracker("urgent") <> 0 Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Item marked as URGENT!"
'        End If
'        If UCase(l_rsDADCTracker("componenttype")) = "AUDIO" And IsNull(l_rsDADCTracker("masterarrived")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Audio File is Missing."
'        End If
'        If UCase(l_rsDADCTracker("componenttype")) = "SUBTITLE" And IsNull(l_rsDADCTracker("masterarrived")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Subititle File is Missing."
'        End If
'        If (UCase(l_rsDADCTracker("componenttype")) = "VIDEO" Or UCase(l_rsDADCTracker("componenttype")) = "TRAILER") And IsNull(l_rsDADCTracker("masterarrived")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Video File is Missing."
'        End If
'        l_strEmailBody = l_strEmailBody & vbCrLf & "Link to Web Tracker: http://online.rrmedia.tv/svensktracker/tracker_details.asp?trackerID=" & l_rsDADCTracker("tracker_svensk_itemID") & "&goback=tracker" & vbCrLf & vbCrLf
'        l_rsDADCTracker.MoveNext
'    Wend
'End If
'l_rsDADCTracker.Close
'
'Debug.Print l_strEmailBody
'
'If l_strEmailBody <> "" Then
'    l_strEmailBody = "The following items have a due date which has passed, please amend due date and advise on delivery of assets:" & vbCrLf & vbCrLf & l_strEmailBody
'    l_strEmailBody = l_strEmailBody & "When accessing the Web Tracker, please be sure to log in with the correct credentials for that particular Tracker." & vbCrLf
'    If l_lngLastProjectManagerContactID <> 0 Then
'        l_strEmailTo = GetData("contact", "email", "contactID", l_lngLastProjectManagerContactID)
'        l_strEmailToName = GetData("contact", "name", "contactID", l_lngLastProjectManagerContactID)
'        SendSMTPMail l_strEmailTo, l_strEmailToName, "Missing Assets � Overdue", l_strEmailBody, "", True, "coreswe@sf.se", "coreswe@sf.se", "", g_strAdministratorEmailAddress
'    Else
'        'Send to coreSF address? coreswe@sf.se
'        SendSMTPMail "coreswe@sf.se", "coreswe@sf.se", "Missing Assets � Overdue", l_strEmailBody, "", True, "", "", "", g_strAdministratorEmailAddress
'    End If
'End If
'
'SectionTitle = "DADC Svensk Tracker Missing Asset Checks : DADC Project Managers - within 5 days of deadline"
'l_strEmailBody = ""
'
'l_strSQL = "SELECT * FROM tracker_svensk_item "
'l_strSQL = l_strSQL & "WHERE assetshere = 0 and duedate <= '" & FormatSQLDate(l_datInterested) & "' AND readytobill = 0 AND billed = 0 AND billed = 0 AND companyID = 1330  "
'l_strSQL = l_strSQL & "ORDER BY projectmanagercontactID, urgent, duedate;"
'Debug.Print l_strSQL
'l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
'If l_rsDADCTracker.RecordCount > 0 Then
'    l_rsDADCTracker.MoveFirst
'    l_lngLastProjectManagerContactID = Val(Trim(" " & l_rsDADCTracker("projectmanagercontactID")))
'    While Not l_rsDADCTracker.EOF
'        If Val(Trim(" " & l_rsDADCTracker("projectmanagercontactID"))) <> l_lngLastProjectManagerContactID Then
'            'Send the email to the last project manager
'            Debug.Print l_strEmailBody
'            If l_strEmailBody <> "" Then
'                l_strEmailBody = "The following items have a due date which has passed, please amend due date and advise on delivery of assets:" & vbCrLf & vbCrLf & l_strEmailBody
'                l_strEmailBody = l_strEmailBody & "When accessing the Web Tracker, please be sure to log in with the correct credentials for that particular Tracker." & vbCrLf
'                If l_lngLastProjectManagerContactID <> 0 Then
'                    l_strEmailTo = GetData("contact", "email", "contactID", l_lngLastProjectManagerContactID)
'                    l_strEmailToName = GetData("contact", "name", "contactID", l_lngLastProjectManagerContactID)
'                    SendSMTPMail l_strEmailTo, l_strEmailToName, "Missing Assets � Overdue", l_strEmailBody, "", True, "", "", "", g_strAdministratorEmailAddress
'                Else
'                    'Send to coreSF address? coreswe@sf.se
'                    SendSMTPMail "SF_Inbound_Material@sonydadc.com", "SF_Inbound_Material@sonydadc.com", "Missing Assets � Overdue", l_strEmailBody, "", True, "", "", "", g_strAdministratorEmailAddress
'                End If
'            End If
'            l_lngLastProjectManagerContactID = l_rsDADCTracker("projectmanagercontactID")
'            l_strEmailBody = ""
'        End If
'        l_strSQL = "SELECT * FROM tracker_svensk_comment WHERE tracker_svensk_itemID = " & l_rsDADCTracker("tracker_svensk_itemID") & "ORDER BY cdate;"
'        l_rsDADCcomments.Open l_strSQL, cnn, 3, 3
'        l_strEmailBody = l_strEmailBody & GetData("company", "name", "companyID", l_rsDADCTracker("companyID"))
'        If Not IsNull(l_rsDADCTracker("itemreference")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Reference: " & l_rsDADCTracker("itemreference")
'        End If
'        If Not IsNull(l_rsDADCTracker("barcode")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Barcode: " & l_rsDADCTracker("barcode")
'        End If
'        If Not IsNull(l_rsDADCTracker("duedate")) Then
'            l_strEmailBody = l_strEmailBody & " Due Date: " & l_rsDADCTracker("duedate")
'        End If
'        l_strEmailBody = l_strEmailBody & vbCrLf & "Title: " & Trim(" " & l_rsDADCTracker("title")) & " - " & Trim(" " & l_rsDADCTracker("programtype"))
'        If Trim(" " & l_rsDADCTracker("series")) <> "" Then l_strEmailBody = l_strEmailBody & " Series #: " & l_rsDADCTracker("series")
'        If Trim(" " & l_rsDADCTracker("episode")) <> "" Then l_strEmailBody = l_strEmailBody & vbCrLf & "Episode #: " & l_rsDADCTracker("episode")
'        If Not IsNull(l_rsDADCTracker("language")) Then
'            l_strEmailBody = l_strEmailBody & " Language: " & l_rsDADCTracker("language")
'        End If
'        If l_rsDADCcomments.RecordCount > 0 Then
'            l_rsDADCcomments.MoveLast
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Last Comment (" & l_rsDADCcomments("cdate") & "): " & l_rsDADCcomments("comment")
'        End If
'        l_rsDADCcomments.Close
'        If l_rsDADCTracker("urgent") <> 0 Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Item marked as URGENT!"
'        End If
'        If UCase(l_rsDADCTracker("componenttype")) = "AUDIO" And IsNull(l_rsDADCTracker("masterarrived")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Audio File is Missing."
'        End If
'        If UCase(l_rsDADCTracker("componenttype")) = "SUBTITLE" And IsNull(l_rsDADCTracker("masterarrived")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Subititle File is Missing."
'        End If
'        If (UCase(l_rsDADCTracker("componenttype")) = "VIDEO" Or UCase(l_rsDADCTracker("componenttype")) = "TRAILER") And IsNull(l_rsDADCTracker("masterarrived")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Video File is Missing."
'        End If
'        l_strEmailBody = l_strEmailBody & vbCrLf & "Link to Web Tracker: http://online.rrmedia.tv/svensktracker/tracker_details.asp?trackerID=" & l_rsDADCTracker("tracker_svensk_itemID") & "&goback=tracker" & vbCrLf & vbCrLf
'        l_rsDADCTracker.MoveNext
'    Wend
'End If
'l_rsDADCTracker.Close
'
'Debug.Print l_strEmailBody
'
'If l_strEmailBody <> "" Then
'    l_strEmailBody = "The following items have a due date which has passed, please amend due date and advise on delivery of assets:" & vbCrLf & vbCrLf & l_strEmailBody
'    l_strEmailBody = l_strEmailBody & "When accessing the Web Tracker, please be sure to log in with the correct credentials for that particular Tracker." & vbCrLf
'    If l_lngLastProjectManagerContactID <> 0 Then
'        l_strEmailTo = GetData("contact", "email", "contactID", l_lngLastProjectManagerContactID)
'        l_strEmailToName = GetData("contact", "name", "contactID", l_lngLastProjectManagerContactID)
'        SendSMTPMail l_strEmailTo, l_strEmailToName, "Missing Assets � Overdue", l_strEmailBody, "", True, "", "", "", g_strAdministratorEmailAddress
'    Else
'        'Send to coreSF address? coreswe@sf.se
'        SendSMTPMail "SF_Inbound_Material@sonydadc.com", "SF_Inbound_Material@sonydadc.com", "Missing Assets � Overdue", l_strEmailBody, "", True, "", "", "", g_strAdministratorEmailAddress
'    End If
'End If
'
'SectionTitle = "DADC Svensk Tracker Missing Asset Checks : DADC and Internal - between 8 and 7 days to deadline"
'l_datInterested = DateAdd("d", 8, Now)
'l_datInterested2 = DateAdd("d", 7, Now)
'l_strEmailBody = ""
'
'l_strSQL = "SELECT * FROM tracker_svensk_item "
'l_strSQL = l_strSQL & "WHERE assetshere = 0 and duedate <= '" & FormatSQLDate(l_datInterested) & "' and duedate > '" & FormatSQLDate(l_datInterested2) & "' AND readytobill = 0 AND billed = 0 AND companyID > 100 ORDER BY urgent, duedate;"
'Debug.Print l_strSQL
'l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
'If l_rsDADCTracker.RecordCount > 0 Then
'    l_rsDADCTracker.MoveFirst
'    While Not l_rsDADCTracker.EOF
'        l_strSQL = "SELECT * FROM tracker_svensk_comment WHERE tracker_svensk_itemID = " & l_rsDADCTracker("tracker_svensk_itemID") & "ORDER BY cdate;"
'        l_rsDADCcomments.Open l_strSQL, cnn, 3, 3
'        l_strEmailBody = l_strEmailBody & GetData("company", "portalcompanyname", "companyID", l_rsDADCTracker("companyID"))
'        If Not IsNull(l_rsDADCTracker("itemreference")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Reference: " & l_rsDADCTracker("itemreference")
'        End If
'        If Not IsNull(l_rsDADCTracker("barcode")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Barcode: " & l_rsDADCTracker("barcode")
'        End If
'        If Not IsNull(l_rsDADCTracker("duedate")) Then
'            l_strEmailBody = l_strEmailBody & " Due Date: " & l_rsDADCTracker("duedate")
'        End If
'        l_strEmailBody = l_strEmailBody & vbCrLf & "Title: " & Trim(" " & l_rsDADCTracker("title")) & " - " & Trim(" " & l_rsDADCTracker("programtype"))
'        If Trim(" " & l_rsDADCTracker("series")) <> "" Then l_strEmailBody = l_strEmailBody & " Series #: " & l_rsDADCTracker("series")
'        If Trim(" " & l_rsDADCTracker("episode")) <> "" Then l_strEmailBody = l_strEmailBody & vbCrLf & "Episode #: " & l_rsDADCTracker("episode")
'        If Not IsNull(l_rsDADCTracker("language")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Language: " & l_rsDADCTracker("language")
'        End If
'        If l_rsDADCcomments.RecordCount > 0 Then
'            l_rsDADCcomments.MoveLast
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Last Comment (" & l_rsDADCcomments("cdate") & "): " & l_rsDADCcomments("comment")
'        End If
'        l_rsDADCcomments.Close
'        If l_rsDADCTracker("urgent") <> 0 Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Item marked as URGENT!"
'        End If
'        If UCase(l_rsDADCTracker("componenttype")) = "AUDIO" And IsNull(l_rsDADCTracker("masterarrived")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Audio File is Missing."
'        End If
'        If UCase(l_rsDADCTracker("componenttype")) = "SUBTITLE" And IsNull(l_rsDADCTracker("masterarrived")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Subititle File is Missing."
'        End If
'        If (UCase(l_rsDADCTracker("componenttype")) = "VIDEO" Or UCase(l_rsDADCTracker("componenttype")) = "TRAILER") And IsNull(l_rsDADCTracker("masterarrived")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Video File is Missing."
'        End If
'        l_strEmailBody = l_strEmailBody & vbCrLf & "Link to Web Tracker: http://online.rrmedia.tv/svensktracker/tracker_details.asp?trackerID=" & l_rsDADCTracker("tracker_svensk_itemID") & "&goback=tracker" & vbCrLf & vbCrLf
'        l_rsDADCTracker.MoveNext
'    Wend
'End If
'l_rsDADCTracker.Close
'
'Debug.Print l_strEmailBody
'
'If l_strEmailBody <> "" Then
'
'    l_strEmailBody = "The following items have Missing Assets, and are approaching due date:" & vbCrLf & vbCrLf & l_strEmailBody
'    l_strEmailBody = l_strEmailBody & "When accessing the Web Tracker, please be sure to log in with the correct credentials for that particular Tracker." & vbCrLf
'    l_strSQL = "SELECT * FROM trackernotification WHERE contractgroup = 'SVENSK' AND trackermessageID = 29;"
'    l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
'    If l_rsDADCTracker.RecordCount > 0 Then
'        l_rsDADCTracker.MoveFirst
'        While Not l_rsDADCTracker.EOF
'            SendSMTPMail l_rsDADCTracker("email"), l_rsDADCTracker("fullname"), "Missing Assets � Warning", l_strEmailBody, "", True, "", "", ""
'            l_rsDADCTracker.MoveNext
'        Wend
'    End If
'    l_rsDADCTracker.Close
'
'End If
'
'SectionTitle = "DADC Svensk Tracker Missing Asset Checks : Svensk Project Managers - between 8 and 7 days to deadline"
'l_strEmailBody = ""
'
'l_strSQL = "SELECT * FROM tracker_svensk_item "
'l_strSQL = l_strSQL & "WHERE assetshere = 0 and duedate <= '" & FormatSQLDate(l_datInterested) & "' and duedate > '" & FormatSQLDate(l_datInterested2) & "' AND readytobill = 0 AND billed = 0 AND companyID = 1415 "
'l_strSQL = l_strSQL & "ORDER BY projectmanagercontactID, urgent, duedate;"
'Debug.Print l_strSQL
'l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
'If l_rsDADCTracker.RecordCount > 0 Then
'    l_rsDADCTracker.MoveFirst
'    l_lngLastProjectManagerContactID = Val(Trim(" " & l_rsDADCTracker("projectmanagercontactID")))
'    While Not l_rsDADCTracker.EOF
'        If Val(Trim(" " & l_rsDADCTracker("projectmanagercontactID"))) <> l_lngLastProjectManagerContactID Then
'            'Send the email to the last project manager
'            Debug.Print l_strEmailBody
'            If l_strEmailBody <> "" Then
'                l_strEmailBody = "The following items have Missing Assets, and are approaching due date:" & vbCrLf & vbCrLf & l_strEmailBody
'                l_strEmailBody = l_strEmailBody & "When accessing the Web Tracker, please be sure to log in with the correct credentials for that particular Tracker." & vbCrLf
'                If l_lngLastProjectManagerContactID <> 0 Then
'                    l_strEmailTo = GetData("contact", "email", "contactID", l_lngLastProjectManagerContactID)
'                    l_strEmailToName = GetData("contact", "name", "contactID", l_lngLastProjectManagerContactID)
'                    SendSMTPMail l_strEmailTo, l_strEmailToName, "Missing Assets � Warning", l_strEmailBody, "", True, "coreswe@sf.se", "coreswe@sf.se", "", g_strAdministratorEmailAddress
'                Else
'                    'Send to coreSF address? coreswe@sf.se
'                    SendSMTPMail "coreswe@sf.se", "coreswe@sf.se", "Missing Assets � Warning", l_strEmailBody, "", True, "", "", "", g_strAdministratorEmailAddress
'                End If
'            End If
'            l_lngLastProjectManagerContactID = l_rsDADCTracker("projectmanagercontactID")
'            l_strEmailBody = ""
'        End If
'        l_strSQL = "SELECT * FROM tracker_svensk_comment WHERE tracker_svensk_itemID = " & l_rsDADCTracker("tracker_svensk_itemID") & "ORDER BY cdate;"
'        l_rsDADCcomments.Open l_strSQL, cnn, 3, 3
'        l_strEmailBody = l_strEmailBody & GetData("company", "name", "companyID", l_rsDADCTracker("companyID"))
'        If Not IsNull(l_rsDADCTracker("itemreference")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Reference: " & l_rsDADCTracker("itemreference")
'        End If
'        If Not IsNull(l_rsDADCTracker("barcode")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Barcode: " & l_rsDADCTracker("barcode")
'        End If
'        If Not IsNull(l_rsDADCTracker("duedate")) Then
'            l_strEmailBody = l_strEmailBody & " Due Date: " & l_rsDADCTracker("duedate")
'        End If
'        l_strEmailBody = l_strEmailBody & vbCrLf & "Title: " & Trim(" " & l_rsDADCTracker("title")) & " - " & Trim(" " & l_rsDADCTracker("programtype"))
'        If Trim(" " & l_rsDADCTracker("series")) <> "" Then l_strEmailBody = l_strEmailBody & " Series #: " & l_rsDADCTracker("series")
'        If Trim(" " & l_rsDADCTracker("episode")) <> "" Then l_strEmailBody = l_strEmailBody & vbCrLf & "Episode #: " & l_rsDADCTracker("episode")
'        If Not IsNull(l_rsDADCTracker("language")) Then
'            l_strEmailBody = l_strEmailBody & " Language: " & l_rsDADCTracker("language")
'        End If
'        If l_rsDADCcomments.RecordCount > 0 Then
'            l_rsDADCcomments.MoveLast
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Last Comment (" & l_rsDADCcomments("cdate") & "): " & l_rsDADCcomments("comment")
'        End If
'        l_rsDADCcomments.Close
'        If l_rsDADCTracker("urgent") <> 0 Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Item marked as URGENT!"
'        End If
'        If UCase(l_rsDADCTracker("componenttype")) = "AUDIO" And IsNull(l_rsDADCTracker("masterarrived")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Audio File is Missing."
'        End If
'        If UCase(l_rsDADCTracker("componenttype")) = "SUBTITLE" And IsNull(l_rsDADCTracker("masterarrived")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Subititle File is Missing."
'        End If
'        If (UCase(l_rsDADCTracker("componenttype")) = "VIDEO" Or UCase(l_rsDADCTracker("componenttype")) = "TRAILER") And IsNull(l_rsDADCTracker("masterarrived")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Video File is Missing."
'        End If
'        l_strEmailBody = l_strEmailBody & vbCrLf & "Link to Web Tracker: http://online.rrmedia.tv/svensktracker/tracker_details.asp?trackerID=" & l_rsDADCTracker("tracker_svensk_itemID") & "&goback=tracker" & vbCrLf & vbCrLf
'        l_rsDADCTracker.MoveNext
'    Wend
'End If
'l_rsDADCTracker.Close
'
'Debug.Print l_strEmailBody
'
'If l_strEmailBody <> "" Then
'    l_strEmailBody = "The following items have Missing Assets, and are approaching due date:" & vbCrLf & vbCrLf & l_strEmailBody
'    l_strEmailBody = l_strEmailBody & "When accessing the Web Tracker, please be sure to log in with the correct credentials for that particular Tracker." & vbCrLf
'    If l_lngLastProjectManagerContactID <> 0 Then
'        l_strEmailTo = GetData("contact", "email", "contactID", l_lngLastProjectManagerContactID)
'        l_strEmailToName = GetData("contact", "name", "contactID", l_lngLastProjectManagerContactID)
'        SendSMTPMail l_strEmailTo, l_strEmailToName, "Missing Assets � Warning", l_strEmailBody, "", True, "coreswe@sf.se", "coreswe@sf.se", "", g_strAdministratorEmailAddress
'    Else
'        'Send to coreSF address? coreswe@sf.se
'        SendSMTPMail "coreswe@sf.se", "coreswe@sf.se", "Missing Assets � Warning", l_strEmailBody, "", True, "", "", "", g_strAdministratorEmailAddress
'    End If
'End If
'
'SectionTitle = "DADC Svensk Tracker Missing Asset Checks : DADC Project Managers - between 8 and 7 days to deadline"
'l_strEmailBody = ""
'
'l_strSQL = "SELECT * FROM tracker_svensk_item "
'l_strSQL = l_strSQL & "WHERE assetshere = 0 and duedate <= '" & FormatSQLDate(l_datInterested) & "' and duedate > '" & FormatSQLDate(l_datInterested2) & "' AND readytobill = 0 AND billed = 0 AND billed = 0 AND companyID = 1330  "
'l_strSQL = l_strSQL & "ORDER BY projectmanagercontactID, urgent, duedate;"
'Debug.Print l_strSQL
'l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
'If l_rsDADCTracker.RecordCount > 0 Then
'    l_rsDADCTracker.MoveFirst
'    l_lngLastProjectManagerContactID = Val(Trim(" " & l_rsDADCTracker("projectmanagercontactID")))
'    While Not l_rsDADCTracker.EOF
'        If Val(Trim(" " & l_rsDADCTracker("projectmanagercontactID"))) <> l_lngLastProjectManagerContactID Then
'            'Send the email to the last project manager
'            Debug.Print l_strEmailBody
'            If l_strEmailBody <> "" Then
'                l_strEmailBody = "The following items have Missing Assets, and are within 8 days of the Due Date:" & vbCrLf & vbCrLf & l_strEmailBody
'                l_strEmailBody = l_strEmailBody & "When accessing the Web Tracker, please be sure to log in with the correct credentials for that particular Tracker." & vbCrLf
'                If l_lngLastProjectManagerContactID <> 0 Then
'                    l_strEmailTo = GetData("contact", "email", "contactID", l_lngLastProjectManagerContactID)
'                    l_strEmailToName = GetData("contact", "name", "contactID", l_lngLastProjectManagerContactID)
'                    SendSMTPMail l_strEmailTo, l_strEmailToName, "DADC Svensk Missing Assets - within 8 days of Due Date", l_strEmailBody, "", True, "", "", "", g_strAdministratorEmailAddress
'                Else
'                    'Send to coreSF address? coreswe@sf.se
'                    SendSMTPMail "SF_Inbound_Material@sonydadc.com", "SF_Inbound_Material@sonydadc.com", "DADC Svensk Missing Assets - within 8 days of Due Date", l_strEmailBody, "", True, "", "", "", g_strAdministratorEmailAddress
'                End If
'            End If
'            l_lngLastProjectManagerContactID = l_rsDADCTracker("projectmanagercontactID")
'            l_strEmailBody = ""
'        End If
'        l_strSQL = "SELECT * FROM tracker_svensk_comment WHERE tracker_svensk_itemID = " & l_rsDADCTracker("tracker_svensk_itemID") & "ORDER BY cdate;"
'        l_rsDADCcomments.Open l_strSQL, cnn, 3, 3
'        l_strEmailBody = l_strEmailBody & GetData("company", "name", "companyID", l_rsDADCTracker("companyID"))
'        If Not IsNull(l_rsDADCTracker("itemreference")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Reference: " & l_rsDADCTracker("itemreference")
'        End If
'        If Not IsNull(l_rsDADCTracker("barcode")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Barcode: " & l_rsDADCTracker("barcode")
'        End If
'        If Not IsNull(l_rsDADCTracker("duedate")) Then
'            l_strEmailBody = l_strEmailBody & " Due Date: " & l_rsDADCTracker("duedate")
'        End If
'        l_strEmailBody = l_strEmailBody & vbCrLf & "Title: " & Trim(" " & l_rsDADCTracker("title")) & " - " & Trim(" " & l_rsDADCTracker("programtype"))
'        If Trim(" " & l_rsDADCTracker("series")) <> "" Then l_strEmailBody = l_strEmailBody & " Series #: " & l_rsDADCTracker("series")
'        If Trim(" " & l_rsDADCTracker("episode")) <> "" Then l_strEmailBody = l_strEmailBody & vbCrLf & "Episode #: " & l_rsDADCTracker("episode")
'        If Not IsNull(l_rsDADCTracker("language")) Then
'            l_strEmailBody = l_strEmailBody & " Language: " & l_rsDADCTracker("language")
'        End If
'        If l_rsDADCcomments.RecordCount > 0 Then
'            l_rsDADCcomments.MoveLast
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Last Comment (" & l_rsDADCcomments("cdate") & "): " & l_rsDADCcomments("comment")
'        End If
'        l_rsDADCcomments.Close
'        If l_rsDADCTracker("urgent") <> 0 Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Item marked as URGENT!"
'        End If
'        If UCase(l_rsDADCTracker("componenttype")) = "AUDIO" And IsNull(l_rsDADCTracker("masterarrived")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Audio File is Missing."
'        End If
'        If UCase(l_rsDADCTracker("componenttype")) = "SUBTITLE" And IsNull(l_rsDADCTracker("masterarrived")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Subititle File is Missing."
'        End If
'        If (UCase(l_rsDADCTracker("componenttype")) = "VIDEO" Or UCase(l_rsDADCTracker("componenttype")) = "TRAILER") And IsNull(l_rsDADCTracker("masterarrived")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Video File is Missing."
'        End If
'        l_strEmailBody = l_strEmailBody & vbCrLf & "Link to Web Tracker: http://online.rrmedia.tv/svensktracker/tracker_details.asp?trackerID=" & l_rsDADCTracker("tracker_svensk_itemID") & "&goback=tracker" & vbCrLf & vbCrLf
'        l_rsDADCTracker.MoveNext
'    Wend
'End If
'l_rsDADCTracker.Close
'
'Debug.Print l_strEmailBody
'
'If l_strEmailBody <> "" Then
'    l_strEmailBody = "The following items have Missing Assets, and are within 8 days of the Due Date:" & vbCrLf & vbCrLf & l_strEmailBody
'    l_strEmailBody = l_strEmailBody & "When accessing the Web Tracker, please be sure to log in with the correct credentials for that particular Tracker." & vbCrLf
'    If l_lngLastProjectManagerContactID <> 0 Then
'        l_strEmailTo = GetData("contact", "email", "contactID", l_lngLastProjectManagerContactID)
'        l_strEmailToName = GetData("contact", "name", "contactID", l_lngLastProjectManagerContactID)
'        SendSMTPMail l_strEmailTo, l_strEmailToName, "DADC Svensk Missing Assets - within 8 days of Due Date", l_strEmailBody, "", True, "", "", "", g_strAdministratorEmailAddress
'    Else
'        'Send to coreSF address? coreswe@sf.se
'        SendSMTPMail "SF_Inbound_Material@sonydadc.com", "SF_Inbound_Material@sonydadc.com", "DADC Svensk Missing Assets - within 8 days of Due Date", l_strEmailBody, "", True, "", "", "", g_strAdministratorEmailAddress
'    End If
'End If
'
'TESTINGPOINT:
'SectionTitle = "DADC iTunes Tracker Missing Asset Checks : Overdue (< 3 weeks)"
'l_datInterested = DateAdd("d", 21, Now)
'l_strEmailBody = ""
'
'l_strSQL = "SELECT * FROM tracker_iTunes_item WHERE "
'l_strSQL = l_strSQL & "assetshere = 0 "
'l_strSQL = l_strSQL & "AND requiredby <= '" & FormatSQLDate(l_datInterested) & "' AND readytobill = 0 AND billed = 0 AND companyID > 100  "
'l_strSQL = l_strSQL & "ORDER BY companyID, priority, requiredby;"
'Debug.Print l_strSQL
'l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
'If l_rsDADCTracker.RecordCount > 0 Then
'    l_rsDADCTracker.MoveFirst
'    l_lngLastCompanyID = l_rsDADCTracker("companyID")
'    While Not l_rsDADCTracker.EOF
'        If l_rsDADCTracker("companyID") <> l_lngLastCompanyID Then
'            'Send the email to the last company
'            Debug.Print l_strEmailBody
'
'            If l_strEmailBody <> "" Then
'                l_strEmailBody = "The following items have Overdue Missing Assets, as they are within 3 weeks of the Due Date:" & vbCrLf & vbCrLf & l_strEmailBody
'                l_strEmailBody = l_strEmailBody & "When accessing the Web Tracker, please be sure to log in with the correct credentials for that particular Tracker." & vbCrLf
'                l_strSQL = "SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", l_lngLastCompanyID) & "' AND trackermessageID = 38;"
'                l_rsEmail.Open l_strSQL, cnn, 3, 3
'                If l_rsEmail.RecordCount > 0 Then
'                    l_rsEmail.MoveFirst
'                    While Not l_rsEmail.EOF
'                        SendSMTPMail l_rsEmail("email"), l_rsEmail("fullname"), "iTunes Missing Assets � Overdue", l_strEmailBody, "", True, "", "", "", g_strAdministratorEmailAddress
'                        l_rsEmail.MoveNext
'                    Wend
'                End If
'                l_lngLastCompanyID = l_rsDADCTracker("companyID")
'                l_strEmailBody = ""
'                l_rsEmail.Close
'            End If
'        End If
'        l_strSQL = "SELECT * FROM tracker_iTunes_comment WHERE tracker_iTunes_itemID = " & l_rsDADCTracker("tracker_iTunes_itemID") & "ORDER BY cdate;"
'        l_rsDADCcomments.Open l_strSQL, cnn, 3, 3
'        l_strEmailBody = l_strEmailBody & GetData("company", "name", "companyID", l_rsDADCTracker("companyID"))
'        If Not IsNull(l_rsDADCTracker("requiredby")) Then
'            l_strEmailBody = l_strEmailBody & " Due Date: " & l_rsDADCTracker("requiredby")
'        End If
'        l_strEmailBody = l_strEmailBody & vbCrLf & "Title: " & Trim(" " & l_rsDADCTracker("title"))
'        If Trim(" " & l_rsDADCTracker("series")) <> "" Then l_strEmailBody = l_strEmailBody & " Series #: " & l_rsDADCTracker("series")
'        If Trim(" " & l_rsDADCTracker("volume")) <> "" Then l_strEmailBody = l_strEmailBody & " Volume #: " & l_rsDADCTracker("volume")
'        If Trim(" " & l_rsDADCTracker("episode")) <> "" Then l_strEmailBody = l_strEmailBody & vbCrLf & "Episode #: " & l_rsDADCTracker("episode")
'        If Trim(" " & l_rsDADCTracker("episodetitle")) <> "" Then l_strEmailBody = l_strEmailBody & vbCrLf & "Episode Title: " & l_rsDADCTracker("episodetitle")
'        If Not IsNull(l_rsDADCTracker("FeatureLanguage1")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Language: " & l_rsDADCTracker("FeatureLanguage1")
'        End If
'        If l_rsDADCcomments.RecordCount > 0 Then
'            l_rsDADCcomments.MoveLast
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Last Comment (" & l_rsDADCcomments("cdate") & "): " & l_rsDADCcomments("comment")
'        End If
'        l_rsDADCcomments.Close
'        If l_rsDADCTracker("priority") <> 0 Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Item marked as URGENT!"
'        End If
'        If (Trim(" " & l_rsDADCTracker("featurelanguage1")) <> "" Or Trim(" " & l_rsDADCTracker("featurelanguage2")) <> "" Or Trim(" " & l_rsDADCTracker("featurelanguage3")) <> "" Or Trim(" " & l_rsDADCTracker("featurelanguage4")) <> "") And Trim(" " & l_rsDADCTracker("mastershere")) = "" Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Feature File is Missing."
'        End If
'        If (Trim(" " & l_rsDADCTracker("trailerlanguage1")) <> "" Or Trim(" " & l_rsDADCTracker("trailerlanguage2")) <> "" Or Trim(" " & l_rsDADCTracker("trailerlanguage3")) <> "" Or Trim(" " & l_rsDADCTracker("trailerlanguage4")) <> "") And Trim(" " & l_rsDADCTracker("TrailerHere")) = "" Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Trailer(s) are Missing."
'        End If
'        If (Trim(" " & l_rsDADCTracker("artworklanguage1")) <> "" Or Trim(" " & l_rsDADCTracker("artworklanguage2")) <> "" Or Trim(" " & l_rsDADCTracker("artworklanguage3")) <> "" Or Trim(" " & l_rsDADCTracker("artworklanguage4")) <> "") And Trim(" " & l_rsDADCTracker("ArtworkHere")) = "" Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Artwork is Missing."
'        End If
'        If (Trim(" " & l_rsDADCTracker("subslanguage1")) <> "" Or Trim(" " & l_rsDADCTracker("subslanguage2")) <> "" Or Trim(" " & l_rsDADCTracker("subslanguage3")) <> "" Or Trim(" " & l_rsDADCTracker("subslanguage4")) <> "") And Trim(" " & l_rsDADCTracker("SubsHere")) = "" Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Subtitle File(s) are Missing."
'        End If
'        If Trim(" " & l_rsDADCTracker("MetadataHere")) = "" Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Metadata is Missing."
'        End If
'        l_strEmailBody = l_strEmailBody & vbCrLf & "Link to Web Tracker: http://online.rrmedia.tv/iTunestracker/tracker_details.asp?trackerID=" & l_rsDADCTracker("tracker_iTunes_itemID") & vbCrLf & vbCrLf
'        l_rsDADCTracker.MoveNext
'    Wend
'End If
'l_rsDADCTracker.Close
'
'Debug.Print l_strEmailBody
'
'If l_strEmailBody <> "" Then
'
'    l_strSQL = "SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", l_lngLastCompanyID) & "' AND trackermessageID = 38;"
'    l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
'    If l_rsDADCTracker.RecordCount > 0 Then
'        l_rsDADCTracker.MoveFirst
'        While Not l_rsDADCTracker.EOF
'            SendSMTPMail l_rsDADCTracker("email"), l_rsDADCTracker("fullname"), "iTunes Missing Assets � Overdue", l_strEmailBody, "", True, "", "", "", g_strAdministratorEmailAddress
'            l_rsDADCTracker.MoveNext
'        Wend
'    End If
'
'    l_rsDADCTracker.Close
'End If
'
'SectionTitle = "DADC iTunes Tracker Missing Asset Checks : Warning  (between 4 weeks and 3 weeks)"
'l_datInterested2 = DateAdd("d", 28, Now)
'l_strEmailBody = ""
'
'l_strSQL = "SELECT * FROM tracker_iTunes_item WHERE "
'l_strSQL = l_strSQL & "assetshere = 0 "
'l_strSQL = l_strSQL & "AND requiredby <= '" & FormatSQLDate(l_datInterested) & "' AND requiredby > '" & FormatSQLDate(l_datInterested2) & "' AND readytobill = 0 AND billed = 0 AND companyID > 100  "
'l_strSQL = l_strSQL & "ORDER BY companyID, priority, requiredby;"
'Debug.Print l_strSQL
'l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
'If l_rsDADCTracker.RecordCount > 0 Then
'    l_rsDADCTracker.MoveFirst
'    l_lngLastCompanyID = l_rsDADCTracker("companyID")
'    While Not l_rsDADCTracker.EOF
'        If l_rsDADCTracker("companyID") <> l_lngLastCompanyID Then
'            'Send the email to the last company
'            Debug.Print l_strEmailBody
'
'            If l_strEmailBody <> "" Then
'                l_strEmailBody = "The following items have Missing Assets, as they are within 4 weeks of the Due Date:" & vbCrLf & vbCrLf & l_strEmailBody
'                l_strEmailBody = l_strEmailBody & "When accessing the Web Tracker, please be sure to log in with the correct credentials for that particular Tracker." & vbCrLf
'                l_strSQL = "SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", l_lngLastCompanyID) & "' AND trackermessageID = 38;"
'                l_rsEmail.Open l_strSQL, cnn, 3, 3
'                If l_rsEmail.RecordCount > 0 Then
'                    l_rsEmail.MoveFirst
'                    While Not l_rsEmail.EOF
'                        SendSMTPMail l_rsEmail("email"), l_rsEmail("fullname"), "iTunes Missing Assets", l_strEmailBody, "", True, "", "", "", g_strAdministratorEmailAddress
'                        l_rsEmail.MoveNext
'                    Wend
'                End If
'                l_lngLastCompanyID = l_rsDADCTracker("companyID")
'                l_strEmailBody = ""
'                l_rsEmail.Close
'            End If
'        End If
'        l_strSQL = "SELECT * FROM tracker_iTunes_comment WHERE tracker_iTunes_itemID = " & l_rsDADCTracker("tracker_iTunes_itemID") & "ORDER BY cdate;"
'        l_rsDADCcomments.Open l_strSQL, cnn, 3, 3
'        l_strEmailBody = l_strEmailBody & GetData("company", "name", "companyID", l_rsDADCTracker("companyID"))
'        If Not IsNull(l_rsDADCTracker("requiredby")) Then
'            l_strEmailBody = l_strEmailBody & " Due Date: " & l_rsDADCTracker("requiredby")
'        End If
'        l_strEmailBody = l_strEmailBody & vbCrLf & "Title: " & Trim(" " & l_rsDADCTracker("title"))
'        If Trim(" " & l_rsDADCTracker("series")) <> "" Then l_strEmailBody = l_strEmailBody & " Series #: " & l_rsDADCTracker("series")
'        If Trim(" " & l_rsDADCTracker("volume")) <> "" Then l_strEmailBody = l_strEmailBody & " Volume #: " & l_rsDADCTracker("volume")
'        If Trim(" " & l_rsDADCTracker("episode")) <> "" Then l_strEmailBody = l_strEmailBody & vbCrLf & "Episode #: " & l_rsDADCTracker("episode")
'        If Trim(" " & l_rsDADCTracker("episodetitle")) <> "" Then l_strEmailBody = l_strEmailBody & vbCrLf & "Episode Title: " & l_rsDADCTracker("episodetitle")
'        If Not IsNull(l_rsDADCTracker("FeatureLanguage1")) Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Language: " & l_rsDADCTracker("FeatureLanguage1")
'        End If
'        If l_rsDADCcomments.RecordCount > 0 Then
'            l_rsDADCcomments.MoveLast
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Last Comment (" & l_rsDADCcomments("cdate") & "): " & l_rsDADCcomments("comment")
'        End If
'        l_rsDADCcomments.Close
'        If l_rsDADCTracker("priority") <> 0 Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "Item marked as URGENT!"
'        End If
'        If (Trim(" " & l_rsDADCTracker("featurelanguage1")) <> "" Or Trim(" " & l_rsDADCTracker("featurelanguage2")) <> "" Or Trim(" " & l_rsDADCTracker("featurelanguage3")) <> "" Or Trim(" " & l_rsDADCTracker("featurelanguage4")) <> "") And Trim(" " & l_rsDADCTracker("mastershere")) = "" Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Feature File is Missing."
'        End If
'        If (Trim(" " & l_rsDADCTracker("trailerlanguage1")) <> "" Or Trim(" " & l_rsDADCTracker("trailerlanguage2")) <> "" Or Trim(" " & l_rsDADCTracker("trailerlanguage3")) <> "" Or Trim(" " & l_rsDADCTracker("trailerlanguage4")) <> "") And Trim(" " & l_rsDADCTracker("TrailerHere")) = "" Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Trailer(s) are Missing."
'        End If
'        If (Trim(" " & l_rsDADCTracker("artworklanguage1")) <> "" Or Trim(" " & l_rsDADCTracker("artworklanguage2")) <> "" Or Trim(" " & l_rsDADCTracker("artworklanguage3")) <> "" Or Trim(" " & l_rsDADCTracker("artworklanguage4")) <> "") And Trim(" " & l_rsDADCTracker("ArtworkHere")) = "" Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Artwork is Missing."
'        End If
'        If (Trim(" " & l_rsDADCTracker("subslanguage1")) <> "" Or Trim(" " & l_rsDADCTracker("subslanguage2")) <> "" Or Trim(" " & l_rsDADCTracker("subslanguage3")) <> "" Or Trim(" " & l_rsDADCTracker("subslanguage4")) <> "") And Trim(" " & l_rsDADCTracker("SubsHere")) = "" Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Subtitle File(s) are Missing."
'        End If
'        If Trim(" " & l_rsDADCTracker("MetadataHere")) = "" Then
'            l_strEmailBody = l_strEmailBody & vbCrLf & "The Metadata is Missing."
'        End If
'        l_strEmailBody = l_strEmailBody & vbCrLf & "Link to Web Tracker: http://online.rrmedia.tv/iTunestracker/tracker_details.asp?trackerID=" & l_rsDADCTracker("tracker_iTunes_itemID") & vbCrLf & vbCrLf
'        l_rsDADCTracker.MoveNext
'    Wend
'End If
'l_rsDADCTracker.Close
'
'Debug.Print l_strEmailBody
'
'If l_strEmailBody <> "" Then
'    l_strEmailBody = "The following items have Missing Assets, as they are within 4 weeks of the Due Date:" & vbCrLf & vbCrLf & l_strEmailBody
'    l_strEmailBody = l_strEmailBody & "When accessing the Web Tracker, please be sure to log in with the correct credentials for that particular Tracker." & vbCrLf
'
'    l_strSQL = "SELECT * FROM trackernotification WHERE contractgroup = '" & GetData("company", "source", "companyID", l_lngLastCompanyID) & "' AND trackermessageID = 38;"
'    l_rsDADCTracker.Open l_strSQL, cnn, 3, 3
'    If l_rsDADCTracker.RecordCount > 0 Then
'        l_rsDADCTracker.MoveFirst
'        While Not l_rsDADCTracker.EOF
'            SendSMTPMail l_rsDADCTracker("email"), l_rsDADCTracker("fullname"), "iTunes Missing Assets", l_strEmailBody, "", True, "", "", "", g_strAdministratorEmailAddress
'            l_rsDADCTracker.MoveNext
'        Wend
'    End If
'
'    l_rsDADCTracker.Close
'End If
'
Set l_rsDADCcomments = Nothing
Set l_rsDADCTracker = Nothing
Set l_rsEmail = Nothing

cnn.Close
Exit Sub

ErrorNotification:

SendSMTPMail g_strAdministratorEmailAddress, "", "DADC Daily Notifications Generated an error", "Error number: " & Err.Number & vbCrLf & "Error Description: " & Err.Description & vbCrLf & "Section: " & SectionTitle, "", True, "", "", ""
End

End Sub

Private Function IsProcessRunning(ByVal sProcess As String) As Boolean

Const MAX_PATH As Long = 260

Dim lProcesses() As Long, lModules() As Long, N As Long, lRet As Long, hProcess As Long
Dim sName As String
        
sProcess = UCase$(sProcess)
ReDim lProcesses(1023) As Long
If EnumProcesses(lProcesses(0), 1024 * 4, lRet) Then
    For N = 0 To (lRet \ 4) - 1
        hProcess = OpenProcess(PROCESS_QUERY_INFORMATION Or PROCESS_VM_READ, 0, lProcesses(N))
        If hProcess Then
            ReDim lModules(1023)
            If EnumProcessModules(hProcess, lModules(0), 1024 * 4, lRet) Then
                sName = String$(MAX_PATH, vbNullChar)
                GetModuleBaseName hProcess, lModules(0), sName, MAX_PATH
                sName = Left$(sName, InStr(sName, vbNullChar) - 1)
                If Len(sName) = Len(sProcess) Then
                    If sProcess = UCase$(sName) Then IsProcessRunning = True: Exit Function
                End If
            End If
        End If
        CloseHandle hProcess
    Next N
End If
End Function
Function QuoteSanitise(lp_strText) As String
        '<EhHeader>
        On Error GoTo QuoteSanitise_Err
        '</EhHeader>


        Dim l_strNewText As String

        Dim l_strOldText As String

        Dim l_intLoop    As Integer

         l_strNewText = ""
    
         If IsNull(lp_strText) Then
            l_strOldText = ""
            l_strNewText = l_strOldText
            lp_strText = ""
        Else
            l_strOldText = lp_strText
        End If
    
        If InStr(lp_strText, "'") <> 0 Then
        
            For l_intLoop = 1 To Len(l_strOldText)
            
                If Mid(l_strOldText, l_intLoop, 1) = "'" Then
                    l_strNewText = l_strNewText & "''"
                Else
                    l_strNewText = l_strNewText & Mid(l_strOldText, l_intLoop, 1)
                End If
            
            Next

        Else
            l_strNewText = lp_strText
        End If
    
        If Right(l_strNewText, 1) = "/" Or Right(l_strNewText, 1) = "\" Then
            l_strNewText = Left(l_strNewText, Len(l_strNewText) - 1)
        End If
    
        QuoteSanitise = l_strNewText
    

        '<EhFooter>
        Exit Function

QuoteSanitise_Err:
        App.LogEvent Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.QuoteSanitise " & _
               "at line " & Erl
        Resume Next
        '</EhFooter>
End Function

Sub SendSMTPMail(ByVal lp_strEmailToAddress As String, ByVal lp_strEmailToName As String, ByVal lp_strSubject As String, ByVal lp_strBody As String, ByVal lp_strAttachment As String, _
ByVal lp_blnDontShowErrors As Boolean, ByVal lp_strCopyToEmailAddress As String, ByVal lp_strCopyToName As String, Optional ByVal lp_strBCCAddress As String, _
Optional lp_blnNoGlobalFooter As Boolean, Optional lp_strEmailFromAddress As String)

Dim SQL As String, cnn2 As ADODB.Connection

If lp_blnNoGlobalFooter = True Then
    'Nothing
Else
    lp_strBody = lp_strBody & vbCrLf & vbCrLf & g_strEmailFooter
End If

SQL = "INSERT INTO emailmessage (emailTo, emailCC, emailBCC, emailSubject, emailBodyPlain, emailAttachmentPath, emailFrom, cuser, muser, SendViaService) VALUES ("
SQL = SQL & "'" & QuoteSanitise(lp_strEmailToAddress) & "', "
SQL = SQL & IIf(lp_strCopyToEmailAddress <> "", "'" & QuoteSanitise(lp_strCopyToEmailAddress) & "', ", "Null, ")
SQL = SQL & IIf(lp_strBCCAddress <> "", "'" & QuoteSanitise(lp_strBCCAddress) & "', ", "Null, ")
SQL = SQL & "'" & QuoteSanitise(lp_strSubject) & "', "
SQL = SQL & "'" & QuoteSanitise(lp_strBody) & "', "
SQL = SQL & IIf(lp_strAttachment <> "", "'" & QuoteSanitise(lp_strAttachment) & "', ", "Null, ")
SQL = SQL & IIf(lp_strEmailFromAddress <> "", "'" & QuoteSanitise(lp_strEmailFromAddress) & "', ", "Null, ")
SQL = SQL & "'Aspera', "
SQL = SQL & "'Aspera', "
SQL = SQL & "1);"

'On Error Resume Next
Debug.Print SQL
Set cnn2 = New ADODB.Connection
cnn2.Open g_strConnection
cnn2.Execute SQL
cnn2.Close
Set cnn2 = Nothing
'On Error GoTo 0

End Sub

Public Function FormatSQLDate(lDate As Variant)
    
    'TVCodeTools ErrorEnablerStart
    On Error GoTo PROC_ERR
    'TVCodeTools ErrorEnablerEnd
    
    If IsNull(lDate) Then
        FormatSQLDate = Null
        Exit Function
    End If
    If lDate = "" Then
        FormatSQLDate = Null
        Exit Function
    End If
    If Not IsDate(lDate) Then
        FormatSQLDate = Null
        Exit Function
    
    End If
    
    FormatSQLDate = Year(lDate) & "/" & Month(lDate) & "/" & Day(lDate) & " " & Hour(lDate) & ":" & Minute(lDate) & ":" & Second(lDate)
    
    'TVCodeTools ErrorHandlerStart
PROC_EXIT:
    Exit Function
    
PROC_ERR:
    App.LogEvent Err.Description
    Resume PROC_EXIT
    'TVCodeTools ErrorHandlerEnd
    
End Function

Function GetData(lp_strTableName As String, lp_strFieldToReturn As String, lp_strFieldToSearch As String, lp_varCriterea As Variant) As Variant
        '<EhHeader>
        On Error GoTo GetData_Err
        '</EhHeader>


    Dim l_strSQL As String
    l_strSQL = "SELECT " & lp_strFieldToReturn & " FROM " & lp_strTableName & " WHERE " & lp_strFieldToSearch & " = '" & (lp_varCriterea) & "'"

    Dim l_rstGetData As New ADODB.Recordset
    Dim l_con As New ADODB.Connection

    l_con.Open g_strConnection
    l_rstGetData.Open l_strSQL, l_con, adOpenStatic, adLockReadOnly

    If Not l_rstGetData.EOF Then
    
        If Not IsNull(l_rstGetData(lp_strFieldToReturn)) Then
            GetData = l_rstGetData(lp_strFieldToReturn)
            GoTo Proc_CloseDB
        End If

    Else
        GoTo Proc_CloseDB

    End If

    Select Case l_rstGetData.Fields(lp_strFieldToReturn).Type
    Case adChar, adVarChar, adVarWChar, 201, 203
        GetData = ""
    Case adBigInt, adBinary, adBoolean, adCurrency, adDecimal, adDouble, adInteger
        GetData = 0
    Case adDate, adDBDate, adDBTime, adDBTimeStamp
        GetData = 0
    Case Else
        GetData = False
    End Select

Proc_CloseDB:

    On Error Resume Next

    l_rstGetData.Close
    Set l_rstGetData = Nothing

    l_con.Close
    Set l_con = Nothing


        '<EhFooter>
        Exit Function

GetData_Err:
        App.LogEvent Err.Description & vbCrLf & _
               "in prjJCAFileManager.modTest.GetData " & _
               "at line " & Erl
        Resume Next
        '</EhFooter>
End Function

Public Function URLEncode(ByVal s As String) As String

    Dim lLoop As Long
    Dim sC As String

    URLEncode = ""
    For lLoop = 1 To Len(s)
        sC = Mid$(s, lLoop, 1)
        If InStr("0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ", UCase$(sC)) = 0 Then
            If Asc(sC) < 16 Then
                sC = "%0" & Hex$(Asc(sC))
            Else
                sC = "%" & Hex$(Asc(sC))
            End If
        End If
        
        URLEncode = URLEncode & sC
    Next
    
End Function

Public Sub Main()

Initialise_cnn
DADCPendingItemsProcess
Beep

End Sub

Public Sub Initialise_cnn()

Dim CommandString As String
CommandString = Command()

g_strConnection = "DRIVER={ODBC Driver 13 for SQL Server};SERVER=lon-bssql-01.office.refine-group.com;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"

'g_strConnection = "DRIVER=SQL Server;SERVER=jca-sql-1.jcatv.co.uk;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"
'g_strConnection = "DRIVER={ODBC Driver 13 for SQL Server};SERVER=MX1TEST-sql-3;Failover_Partner=MX1TEST-sql-4;DATABASE=cetasoft;UID=Web_User;PWD=Web_User_Password1903"

Set cnn = New ADODB.Connection
       
End Sub


